# esii3-seforsor-perimetral

## Introduction
This project consists of a network testbed for educational purpouses only. It is
intended to be executed on a virtual machine using the root user (i.e. Kali). Do not
execute this on a production environment or your host operating system directly.

## Deployment
You can run the deploy.sh to deploy all nodes and networks.

The networks deployed will be:

 * internet
 * dmznet
 * intranet

The nodes deployed will be:

 * router1 (connected to internet and dmznet)
 * ids (connected to dmznet)
 * web (connected to dmznet)
 * proxy (connected to dmznet)
 * foreign (connected to dmznet)
 * router2 (connected to intranet and dmznet)
 * hostA (connected to intranet)
 * hostB (connected to intranet)
 * hostC (connected to intranet)
 * hostD (connected to intranet)

Consider internet as a kind of fake Internet to simulate the real Internet. All
nodes have also configured a default route via X.X.X.254; that is the IP of the
bridge created by Docker to handle Docker networks, and can be used to access
the real Internet in case you to install something on a container for example.

Diagram:

                                +-------------+             -------------
                                | FOREIGN     |          --/             \--
                                |             |         /                   \
                                |     10.0.0.1|        (      INTERNET       )
                                |         eth0+---------+      (fake)       /
                                +-------------+          --\             /--
                                                            ------+------
                                                                  |
                                                                  |
                                                           +------+-------+
                                                           |    eth1      |
     +-------------+   +-------------+   +-------------+   |  10.0.0.1    |
     | IDS         |   | WEB         |   | PROXY       |   |              |
     |             |   |             |   |             |   |   ROUTER1    |
     | 172.16.10.4 |   | 172.16.10.5 |   | 172.16.10.6 |   |              |
     |    eth0     |   |    eth0     |   |    eth0     |   | 172.16.10.1  |
     +------+------+   +------+------+   +-----+-------+   |    eth0      |
            |                 |                 |          +------+-------+
            |                 |                 |                 |
            |                 |                 |                 |
      +-----+-----------------+-----------------+-----------------+------+
      |                                                                  |
      |                    DMZNET 172.16.10.0/24                         |
      |                                                                  |
      +--------------------------------+---------------------------------+
                                       |
                                       |
                                       |
                                +------+-------+
                                |    eth0      |
                                | 172.16.10.2  |
                                |              |
                                |   ROUTER2    |
                                |              |
                                | 172.16.12.1  |
                                |    eth1      |
                                +------+-------+
                                       |
                                       |
                                       |
      +--------------------------------+---------------------------------+
      |                                                                  |
      |                   INTRANET 172.16.12.0/24                        |
      |                                                                  |
      +-----+-----------------+-----------------+-----------------+------+
            |                 |                 |                 |
            |                 |                 |                 |
            |                 |                 |                 |
     +------+------+   +------+------+   +------+------+   +------+------+
     |    eth0     |   |    eth0     |   |    eth0     |   |    eth0     |
     | 172.16.12.3 |   | 172.16.12.4 |   | 172.16.12.5 |   | 172.16.12.6 |
     |             |   |             |   |             |   |             |
     | HOSTA       |   | HOSTB       |   | HOSTC       |   | HOSTD       |
     +-------------+   +-------------+   +-------------+   +-------------+

## Use
Once you have this project cloned you can run:

    root@kali:~/esii3-seforsor-perimetral# ./deploy.sh

To get container shell, for example proxy, type:

    root@kali:~/esii3-seforsor-perimetral# docker exec -it proxy /bin/sh

Once have the container shell, it can be checked if a remote service can be
reached. If a service on a specific port of a remote host can be accessed
a message like this is displayed:

    proxy:~# nc -n -z -v 172.16.10.5 80
    172.16.10.5 (172.16.10.5:80) open

If a service on a specific port of a remote host cannot be accessed
nothing is displayed:

    proxy:~# nc -n -z -v 172.16.10.5 8080
    proxy:~#

However, a non zero code is returned. Something like this can be typed to show
explicitly that the connection was refused:

    proxy:~# nc -n -z -v 172.16.10.5 8080 || echo "Connection refused"
    Connection refused

To a reach the different nodes in the different networks iptables rules can be
added to router1 and router2.

Finally, to destroy the whole environment you can type:

    root@kali:~/esii3-seforsor-perimetral# docker-compose down
